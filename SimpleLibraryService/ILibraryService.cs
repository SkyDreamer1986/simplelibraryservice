﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data;
using System.Data.Sql;

namespace SimpleLibraryService
{
    [ServiceContract]
    public interface ILibraryService
    {
        [OperationContract]
        Guid addBook(string caption, string author, string description);

        [OperationContract]
        bool deleteBook(Guid idBook);

        [OperationContract]
        bool editBook(Guid id, string newCaption, string newAuthor, string newDescription);

        [OperationContract]
        DataTable getBookList(string filter);

        [OperationContract]
        string getBookDescription(Guid id);
    }
}
