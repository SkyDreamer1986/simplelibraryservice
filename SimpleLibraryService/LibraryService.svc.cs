﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data.SqlClient;
using System.Data.Common;
using System.Data;
using System.Data.Sql;

namespace SimpleLibraryService
{
    public class LibraryService : ILibraryService
    {
        private bool executeNonQueryCommand(string commandText, CommandType cmdType, SqlParameter[] cmdParams)
        {
            SqlConnection conn = new SqlConnection(Properties.Settings.Default.libraryConnString);
            DataTable dt = new DataTable();
            SqlDataAdapter da;
            SqlCommand cmd = new SqlCommand();
            int result = 0;

            try
            {
                conn.Open();
                cmd = conn.CreateCommand();
                cmd.CommandText = commandText;
                cmd.CommandType = cmdType;
                cmd.Parameters.AddRange(cmdParams);

                result = cmd.ExecuteNonQuery();
            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                cmd = null;
                conn.Close();
            }

            return result > 0;
        }

        private DataTable executeQueryCommand(string commandText, CommandType cmdType, SqlParameter[] cmdParams)
        {
            SqlConnection conn = new SqlConnection(Properties.Settings.Default.libraryConnString);
            DataTable dt = new DataTable("queryResult"+new Guid().ToString());
            SqlDataAdapter da;
            SqlCommand cmd = new SqlCommand();

            try
            {
                conn.Open();
                cmd = conn.CreateCommand();
                cmd.CommandText = commandText;
                cmd.CommandType = cmdType;
                cmd.Parameters.AddRange(cmdParams);

                using (da = new SqlDataAdapter(cmd))
                    da.Fill(dt);
            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                cmd = null;
                conn.Close();
            }

            return dt;
        }

        public Guid addBook(string caption, string author, string description)
        {
            SqlParameter p0 = new SqlParameter("@caption", caption);
            SqlParameter p1 = new SqlParameter("@author", author);
            SqlParameter p2 = new SqlParameter("@description", description);

            DataTable dt = executeQueryCommand("addBook", CommandType.StoredProcedure, new SqlParameter[] { p0, p1, p2 });

            return (Guid)dt.Rows[0]["ID"];
        }

        public bool deleteBook(Guid idBook)
        {
            SqlParameter p = new SqlParameter("@id", idBook);
            return executeNonQueryCommand("deleteBook", CommandType.StoredProcedure, new SqlParameter[] {p});
        }

        public bool editBook(Guid id, string newCaption, string newAuthor, string newDescription)
        {

            SqlParameter p0 = new SqlParameter("@newCaption", newCaption);
            SqlParameter p1 = new SqlParameter("@newAuthor", newAuthor);
            SqlParameter p2 = new SqlParameter("@newDescription", newDescription);
            SqlParameter p3 = new SqlParameter("@id", id);

            return executeNonQueryCommand("editBook", CommandType.StoredProcedure, new SqlParameter[] { p0, p1, p2, p3 });
        }

        public DataTable getBookList(string filter)
        {
            SqlParameter p = new SqlParameter("@filter", filter);
            return executeQueryCommand("getBooks", CommandType.StoredProcedure, new SqlParameter[] {p});
        }
        
        public string getBookDescription(Guid id)
        {
            SqlParameter p0 = new SqlParameter("@id", id.ToString());
            
            DataTable dt = executeQueryCommand("getBookDescription", CommandType.StoredProcedure, new SqlParameter[] { p0});

            return dt.Rows[0]["Description"].ToString();
        }
    }
}
